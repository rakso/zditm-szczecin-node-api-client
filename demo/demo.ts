import { GenericClient, Displays, Vehicles, Stops, Lines } from "../src/index";

function showRateLimit(apiResponse: GenericClient) {
  console.log(
    `- Available requets rate limit ${apiResponse.availableRateLimit} (of ${apiResponse.rateLimit}) -`
  );
}

const displayApi = new Displays({ userAgent: "Nodejs-API-client-byOskar" });
const stopId = 30622;
displayApi.getDisplayData(stopId).then(({ stop_name, departures }) => {
  showRateLimit(displayApi);

  if (departures.length) {
    console.log(
      `From the ${stop_name} stop line ${departures[0].line_number} will soon depart.`
    );
  } else {
    console.log(`Nothing is going to depart from ${stop_name}.`);
  }
});

const vehiclesApi = new Vehicles({ apiVersion: "v1" });
vehiclesApi.getVehiclesData().then(({ data: [{ line_number, next_stop }] }) => {
  showRateLimit(vehiclesApi);

  console.log(`Vehicle ${line_number} travels to the ${next_stop}.`);
});

const stopsApi = new Stops();
stopsApi
  .getStopsData()
  .then(({ data: [{ number, name, latitude, longitude }] }) => {
    showRateLimit(stopsApi);

    console.log(
      `A stop ${name} (${number}) is located at ${latitude} ${longitude}.`
    );
  });

const linesApi = new Lines();
linesApi.getLinesData().then(({ data: [{ id, number, type }] }) => {
  showRateLimit(linesApi);

  console.log(
    `A line with id ${id} has number ${number} and its a type of ${type}.`
  );
});
