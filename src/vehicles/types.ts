import { LineSubType, LineType } from "../lines/types";

export enum VehicleType {
  /**
   * pociąg SKM
   */
  SKM = "skm",
  /**
   * tramwaj
   */
  TRAM = "tram",
  /**
   * autobus
   */
  BUS = "bus",
}

export type VehicleData = {
  /**
   * identyfikator linii (niezmienny)
   */
  line_id: number;
  /**
   * oznaczenie (numer) linii (może ulegać zmianom)
   */
  line_number: string;
  /**
   * typ linii
   */
  line_type: LineType;
  /**
   * podtyp linii
   */
  line_subtype: LineSubType;
  /**
   * rodzaj trakcji
   */
  vehicle_type: VehicleType;
  /**
   * identyfikator pojazdu
   */
  vehicle_id: number;
  /**
   * numer taborowy pojazdu
   */
  vehicle_number: string;
  /**
   * numer trasy danej linii, na której pojazd realizuje obecnie kurs
   */
  route_variant_number: number;
  /**
   * oznaczenie zadania (brygady) realizowanego przez pojazd
   */
  service: string;
  /**
   * nazwa kierunku dla kursu realizowanego obecnie przez pojazd
   */
  direction: string | null;
  /**
   * nazwa poprzedniego przystanku
   */
  previous_stop: string | null;
  /**
   * nazwa kolejnego przystanku
   */
  next_stop: string | null;
  /**
   * współrzędne geograficzne pojazdu (szerokość geograficzna)
   */
  latitude: number;
  /**
   * współrzędne geograficzne pojazdu (długość geograficzna)
   */
  longitude: number;
  /**
   * prędkość chwilowa pojazdu w km/h
   */
  velocity: number;
  /**
   * odstępstwo od rozkładu jazdy w minutach (wartość 0 oznacza kurs realizowany zgodnie z rozkładem jazdy, wartość mniejsza od 0 oznacza kurs opóźniony, a wartość większa od 0 – kurs przyspieszony)
   */
  punctuality: number;
  /**
   * moment ostatniej aktualizacji danych
   */
  updated_at: Date;
};

export type VehiclesData = {
  data: VehicleData[];
};
