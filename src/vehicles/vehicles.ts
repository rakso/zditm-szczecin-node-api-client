import { GenericClient } from "../generic-client/generic-client";
import { VehicleData, VehiclesData } from "./types";

/**
 *  API – pojazdy
 *
 * @url https://www.zditm.szczecin.pl/pl/zditm/dla-programistow/api-pojazdy
 */
export class Vehicles extends GenericClient {
  private endpoint = "vehicles";

  /**
   * Informacje o bieżącej lokalizacji pojazdów komunikacji miejskiej.
   *
   * Częstotliwość aktualizacji: co ok. 30 sekund
   * @returns VehiclesData
   */
  async getVehiclesData(): Promise<VehiclesData> {
    const { data } = await this.doRequest(this.endpoint);

    const vehiclesData: VehicleData[] = [];

    for await (const vehicleDataResponse of data) {
      const vehicleData: VehicleData = {
        line_id: vehicleDataResponse.line_id,
        line_number: vehicleDataResponse.line_number,
        line_type: vehicleDataResponse.line_type,
        line_subtype: vehicleDataResponse.line_subtype,
        vehicle_type: vehicleDataResponse.vehicle_type,
        vehicle_id: vehicleDataResponse.vehicle_id,
        vehicle_number: vehicleDataResponse.vehicle_number,
        route_variant_number: vehicleDataResponse.route_variant_number,
        service: vehicleDataResponse.service,
        direction: vehicleDataResponse.direction,
        previous_stop: vehicleDataResponse.previous_stop,
        next_stop: vehicleDataResponse.next_stop,
        latitude: vehicleDataResponse.latitude,
        longitude: vehicleDataResponse.longitude,
        velocity: vehicleDataResponse.velocity,
        punctuality: vehicleDataResponse.punctuality,
        updated_at: new Date(vehicleDataResponse.updated_at),
      };

      vehiclesData.push(vehicleData);
    }

    const result: VehiclesData = {
      data: vehiclesData,
    };
    return result;
  }
}
