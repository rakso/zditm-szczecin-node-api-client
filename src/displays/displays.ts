import { GenericClient } from "../generic-client/generic-client";
import { Departure, DisplayData } from "./types";

/**
 *  API – tablice odjazdów
 *
 * @url https://www.zditm.szczecin.pl/pl/zditm/dla-programistow/api-tablice-odjazdow
 */
export class Displays extends GenericClient {
  private endpoint = "displays";

  /**
   * Informacje o najbliższych odjazdach z przystanku komunikacji miejskiej (wirtualna tablica).
   *
   * Częstotliwość aktualizacji: co ok. 20 sekund
   * @param stopNumber wartość parametru {stopNumber} można uzyskać korzystając z API dla przystanków
   * @returns DisplayData
   */
  async getDisplayData(stopNumber: number): Promise<DisplayData> {
    const response = await this.doRequest(`${this.endpoint}/${stopNumber}`);

    const departures: Departure[] = [];

    for await (const departureResponse of response.departures) {
      const departure: Departure = {
        line_number: departureResponse.line_number,
        direction: departureResponse.direction,
        time_real: departureResponse.time_real,
        time_scheduled: departureResponse.time_scheduled,
      };

      departures.push(departure);
    }

    const result: DisplayData = {
      stop_name: response.stop_name,
      stop_number: response.stop_name,
      departures,
      message: response.message,
      updated_at: new Date(response.updated_at),
    };
    return result;
  }
}
