export type Departure = {
  /**
   * oznaczenie (numer) linii
   */
  line_number: string;
  /**
   * nazwa kierunku
   */
  direction: string;
  /**
   * rzeczywisty czas do odjazdu w minutach (jeśli to pole nie zawiera wartości `null`, to pole time_scheduled zawiera wartość `null`)
   */
  time_real: number | null;
  /**
   * rozkładowa godzina odjazdu w formacie hh:mm (jeśli to pole nie zawiera wartości `null`, to pole time_real zawiera wartość `null`)
   */
  time_scheduled: string;
};

export type DisplayData = {
  /**
   * nazwa przystanku
   */
  stop_name: string;
  /**
   * numer przystanku
   */
  stop_number: string;
  /**
   * tablica zawierająca listę najbliższych odjazdów z przystanku
   */
  departures: Departure[];
  /**
   * komunikaty tekstowe umieszczone na tablicy
   */
  message: string | null;
  /**
   * moment ostatniej aktualizacji danych
   */
  updated_at: Date;
};
