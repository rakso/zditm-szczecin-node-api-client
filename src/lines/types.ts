import { VehicleType } from "../vehicles/types";

export enum LineType {
  /**
   * linia dzienna
   */
  DAY = "day",
  /**
   *  linia nocna
   */
  NIGHT = "night ",
}

export enum LineSubType {
  /**
   * linia zwykła
   */
  NORMAL = "normal",
  /**
   * linia przyspieszona
   */
  SEMI_FATST = "semi-fast",
  /**
   * linia pospieszna
   */
  FAST = "fast",
  /**
   * linia zastępcza
   */
  REPLACEMENT = "replacement",
  /**
   * linia dodatkowa
   */
  ADDITIONAL = "additional",
  /**
   * linia specjalna
   */
  SPECIAL = "special",
  /**
   *  linia turystyczna
   */
  TOURIST = "tourist",
}

export type LineData = {
  /**
   * identyfikator linii (niezmienny)
   */
  id: number;
  /**
   * oznaczenie (numer) linii (może ulegać zmianom)
   */
  number: string;
  /**
   * typ linii
   */
  type: LineType;
  /**
   * podtyp linii
   */
  subtype: LineSubType;
  /**
   * rodzaj trakcji
   */
  vehicle_type: VehicleType;
  /**
   * wartość true, jeśli linia funkcjonuje w ramach systemu transportu na żądanie
   */
  on_demand: boolean;
  /**
   * wartość true, jeśli linia wyróżniona jest na materiałach informacji pasażerskiej pomarańczowym kolorem (kursuje na zmienionej trasie lub jest linią zastępczą)
   */
  highlighted: boolean;
  /**
   * moment ostatniej aktualizacji danych
   */
  updated_at: Date;
};

export type LinesData = {
  data: LineData[];
};
