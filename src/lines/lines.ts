import { GenericClient } from "../generic-client/generic-client";
import { LineData, LinesData } from "./types";

/**
 *  API – linie
 *
 * @url https://www.zditm.szczecin.pl/pl/zditm/dla-programistow/api-linie
 */
export class Lines extends GenericClient {
  private endpoint = "lines";

  /**
   * Informacje o liniach komunikacji miejskiej.
   *
   * Częstotliwość aktualizacji: w miarę potrzeb
   * @returns LinesData
   */
  async getLinesData(): Promise<LinesData> {
    const { data } = await this.doRequest(this.endpoint);

    const linesData: LineData[] = [];

    for await (const {
      id,
      number,
      type,
      subtype,
      vehicle_type,
      on_demand,
      highlighted,
      updated_at,
    } of data) {
      const lineData: LineData = {
        id,
        number,
        type,
        subtype,
        vehicle_type,
        on_demand,
        highlighted,
        updated_at: new Date(updated_at),
      };

      linesData.push(lineData);
    }

    const result: LinesData = {
      data: linesData,
    };
    return result;
  }
}
