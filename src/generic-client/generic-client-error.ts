export class GenericClientError extends Error {
  constructor(msg: string) {
    super(msg);

    Object.setPrototypeOf(this, GenericClientError.prototype);
  }
}
