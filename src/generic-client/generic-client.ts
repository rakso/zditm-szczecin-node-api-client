import axios, { AxiosError } from "axios";
import { GenericClientOptions } from "./generic-client-options.interface";
import { GenericClientError } from "./generic-client-error";

export class GenericClient {
  private apiHost: string;
  private apiUrl: string;

  private userAgent: string;

  private wholeRatelimit = -1;
  private remainingRatelimit = -1;

  constructor(options: GenericClientOptions = {}) {
    this.apiHost = options.apiHost || "https://www.zditm.szczecin.pl/api";
    this.apiUrl = `${this.apiHost}/${options.apiVersion || "v1"}`;
    this.userAgent = options.userAgent || "Nodejs-API-client";
  }

  protected async doRequest(endpoint: string) {
    const url = `${this.apiUrl}/${endpoint}`;

    const headers = {
      "User-Agent": this.userAgent,
    };

    return axios
      .get(url, { headers })
      .then((response) => {
        this.wholeRatelimit = response.headers["x-ratelimit-limit"];
        this.remainingRatelimit = response.headers["x-ratelimit-remaining"];

        return response.data;
      })
      .catch((reason) => {
        if (reason instanceof AxiosError) {
          if (reason.status === 429) {
            // Nagłówek odpowiedzi X-RateLimit-Reset zawiera uniksowy timestamp równy czasowi zresetowania limitu.
            const limitResetAt = reason.response?.headers["x-ratelimit-limit"];
            // Retry-After zawiera liczbę sekund pozostałych do zresetowania limitu.
            const limitUntil = reason.response?.headers["Retry-After"];
            throw new GenericClientError(
              `The ${this.rateLimit} request limit has been exceeded. You need to wait ${limitUntil}s (until ${limitResetAt}) to renew your limit.`
            );
          }
        } else {
          throw new GenericClientError(
            `Request to ZDiTM API faild. Error: ${reason}`
          );
        }
      });
  }

  get rateLimit() {
    return this.wholeRatelimit;
  }

  get availableRateLimit() {
    return this.remainingRatelimit;
  }
}
