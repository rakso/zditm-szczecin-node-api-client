export interface GenericClientOptions {
  /**
   * Default: https://www.zditm.szczecin.pl/api
   */
  apiHost?: string;
  /**
   * Default: v1
   */
  apiVersion?: string;
  /**
   * Default: Nodejs-API-client
   */
  userAgent?: string;
}
