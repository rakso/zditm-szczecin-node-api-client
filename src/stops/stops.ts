import { GenericClient } from "../generic-client/generic-client";
import { StopData, StopsData } from "./types";

/**
 *  API – przystanki
 *
 * @url https://www.zditm.szczecin.pl/pl/zditm/dla-programistow/api-przystanki
 */
export class Stops extends GenericClient {
  private endpoint = "stops";

  /**
   * Informacje o przystankach komunikacji miejskiej.
   *
   * Częstotliwość aktualizacji: w miarę potrzeb
   * @returns StopsData
   */
  async getStopsData(): Promise<StopsData> {
    const { data } = await this.doRequest(this.endpoint);

    const stopsData: StopData[] = [];

    for await (const stopsDataResponse of data) {
      const stopData: StopData = {
        id: stopsDataResponse.id,
        number: stopsDataResponse.number,
        name: stopsDataResponse.name,
        latitude: stopsDataResponse.latitude,
        longitude: stopsDataResponse.longitude,
        request_stop: stopsDataResponse.request_stop,
        park_and_ride: stopsDataResponse.park_and_ride,
        updated_at: new Date(stopsDataResponse.updated_at),
      };

      stopsData.push(stopData);
    }

    const result: StopsData = {
      data: stopsData,
    };
    return result;
  }
}
