export type StopData = {
  /**
   * identyfikator przystanku (niezmienny)
   */
  id: number;
  /**
   * numer przystanku (może ulegać zmianom)
   */
  number: string;
  /**
   * nazwa przystanku
   */
  name: string;
  /**
   * współrzędne geograficzne przystanku (szerokość geograficzna)
   */
  latitude: number;
  /**
   * współrzędne geograficzne przystanku (długość geograficzna)
   */
  longitude: number;
  /**
   * wartość true, jeśli domyślny status przystanku to „na żądanie”
   */
  request_stop: boolean;
  /**
   * wartość true, jeśli w sąsiedztwie przystanku znajduje się ogólnodostępny parking Park & Ride
   */
  park_and_ride: boolean;
  /**
   * moment ostatniej aktualizacji danych
   */
  updated_at: Date;
};

export type StopsData = {
  data: StopData[];
};
