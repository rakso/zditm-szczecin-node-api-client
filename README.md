# ZDiTM Szczecin - Node.js API client

Unofficial Node.js API client based on "[Dla programistów](https://www.zditm.szczecin.pl/pl/zditm/dla-programistow)" docs webpage.

### APIs currently supported by this client

- [x] `stops` - Information about public transportation stops.
- [x] `lines` - Information about public transport lines.
- [ ] `trajectories` - Information on the geographic routing of public transportation lines.
- [x] `vehicles` - Information on the current location of public transport vehicles.
- [x] `displays` - Information about the nearest departures from the public transport stop (virtual board).
- [ ] `timetable-change-descriptions` - Information on recent and upcoming schedule changes.
- [ ] `ticket-offices` - Ticket office information.
- [ ] `ticket-machines` - Information about stationary ticket vending machines.

### Getting started

Install with:

```bash
npm i zditm-szczecin-node-api-client
```

Then you can use it like this:

```typescript
import {
  GenericClient,
  Displays,
  Vehicles,
  Stops,
  Lines,
} from "zditm-szczecin-node-api-client";

function showRateLimit(apiResponse: GenericClient) {
  console.log(
    `- Available requets rate limit ${apiResponse.availableRateLimit} (of ${apiResponse.rateLimit}) -`
  );
}

const displayApi = new Displays({ userAgent: "Nodejs-API-client-byOskar" });
const stopId = 30622;
displayApi.getDisplayData(stopId).then(({ stop_name, departures }) => {
  showRateLimit(displayApi);

  if (departures.length) {
    console.log(
      `From the ${stop_name} stop line ${departures[0].line_number} will soon depart.`
    );
  } else {
    console.log(`Nothing is going to depart from ${stop_name}.`);
  }
});

const vehiclesApi = new Vehicles({ apiVersion: "v1" });
vehiclesApi.getVehiclesData().then(({ data: [{ line_number, next_stop }] }) => {
  showRateLimit(vehiclesApi);

  console.log(`Vehicle ${line_number} travels to the ${next_stop}.`);
});

const stopsApi = new Stops();
stopsApi
  .getStopsData()
  .then(({ data: [{ number, name, latitude, longitude }] }) => {
    showRateLimit(stopsApi);

    console.log(
      `A stop ${name} (${number}) is located at ${latitude} ${longitude}.`
    );
  });

const linesApi = new Lines();
linesApi.getLinesData().then(({ data: [{ id, number, type }] }) => {
  showRateLimit(linesApi);

  console.log(
    `A line with id ${id} has number ${number} and its a type of ${type}.`
  );
});
```

### Developing the package

_Used node version [v20.4.0](https://nodejs.org/dist/v20.4.0/)._

To run the demo file:

```bash
git clone https://gitlab.com/rakso/zditm-szczecin-node-api-client.git
npm i
npm run demo
```
