# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/0.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [2.0.1] - 2024-04-06

### Added

- Support for: `lines`.

## [1.0.0] - 2023-09-12

### Added

- Support for: `stops`.

### Fixed

- A small typo.

## [0.1.0] - 2023-07-21

### Added

- Support for: `vehicles` and `displays`.

[Unreleased]: https://gitlab.com/rakso/zditm-szczecin-node-api-client/-/compare/v2.0.1...main
[2.0.1]: https://gitlab.com/rakso/zditm-szczecin-node-api-client/-/compare/v1.0.0...v2.0.1
[1.0.0]: https://gitlab.com/rakso/zditm-szczecin-node-api-client/-/compare/v0.1.0...v1.0.0
[0.1.0]: https://gitlab.com/rakso/zditm-szczecin-node-api-client/-/tags/v0.1.0

###### Based on [keep a changelog](https://keepachangelog.com/en/1.1.0/)
